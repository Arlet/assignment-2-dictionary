%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define MAX_SIZE 255
%define DATA_SHIFT 8

global _start
section .bss
    buffer: times MAX_SIZE resb 0
section .data
    no_any_keys: db "No any keys were found.", 0
    key_is_found: db "Value on this key was found: ", 0

section .text
    read_text:
        push r12
        xor r12, r12
        .loop:
            call read_char

            test rax, rax
            jz .end

            mov byte[buffer+r12], al
            inc r12

            jmp .loop
        
        .end:
            mov byte[buffer+r12], 0x0
            pop r12
            ret
    _start:
        call read_text

        mov rdi, buffer

        mov rsi, test2

        call find_word

        test rax, rax

        jz .fault

        push rax

        mov rdi, key_is_found
        call print_string

        pop r11

        mov rdi, r11

        add rdi, DATA_SHIFT

        call string_length ; получение количества символов в ключе

        add rax, DATA_SHIFT ; сдвиг из-за метки на следующее значение
        add rax, r11 ; адрес начала вхождения
        inc rax ; 0-терминатор

        mov rdi, rax
        call print_string
        jmp .end

        .fault:
            mov rdi, no_any_keys
            call print_string

        .end:
            call print_newline

            mov rdi, 0
            call exit


