%include "lib.inc"
%define DATA_SHIFT 8

global find_word


section .data


section .text

    ; первый параметр - указатель на 0-терм строку
    ; второй параметр - указатель на начало словаря
    find_word:
        .loop:
            test rsi, rsi

            jz .fault

            push rdi
            push rsi

            add rsi, DATA_SHIFT

            call string_equals

            pop rsi
            pop rdi

            test rax, rax

            jnz .end

            mov rsi, [rsi]

            jmp .loop
        .end:
            mov rax, rsi
            ret
        .fault:
            xor rax, rax
            ret