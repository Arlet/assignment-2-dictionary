ASM = nasm
ASMFLAGS = -felf64 -g
LD=ld

program: main.o words.o dict.o lib.o
	$(LD) -o $@ $^

dict.o: dict.asm lib.o
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm words.o lib.o dict.o
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	$(RM) *.o
	$(RM) program

.PHONY: clean